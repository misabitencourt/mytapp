

module.exports = (Sequelize, sequelize) => {
    class ApiCache extends Sequelize.Model {}

    ApiCache.init({
        apiK: Sequelize.STRING,
        apiV: Sequelize.STRING
    }, { sequelize, modelName: 'mta_apiCache' });
      
    sequelize.sync();
    
    return ApiCache;
}