const Sequelize = require('sequelize');
const env = require('dotenv').config().parsed;
const sequelize = new Sequelize(`postgres://${env.dbUser}:${env.dbPass}@${env.dbUrl}:5432/${env.dbName}`);
const User = require('./User');
const ApiCache = require('./ApiCache');

module.exports = {
    User: User(Sequelize, sequelize),
    ApiCache: ApiCache(Sequelize, sequelize)
};

