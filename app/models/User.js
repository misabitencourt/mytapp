

module.exports = (Sequelize, sequelize) => {
    class User extends Sequelize.Model {}

    User.init({
        email: Sequelize.STRING,
        password: Sequelize.STRING,
        token: Sequelize.STRING,
        name: Sequelize.STRING,
        birthday: Sequelize.DATE
    }, { sequelize, modelName: 'mta_user' });
      
    sequelize.sync()
    .then(() => {
        User.findAll({ limit: 1 }).then(users => {
            if (! users.length) {
                User.create({
                    email: 'misabitencourt@gmail.com',
                    password: 'testeteste', // FIXME sha2
                    token: '',
                    name: 'Misael Braga de Bitencourt',
                    birthday: new Date(1989, 6, 26)
                });
            }
        });
    })
    .then(person => person && console.log(person.toJSON()));
    
    return User;
}