const db = require('../models/index');
const fetch = require('node-fetch');

module.exports = {

    async getDrinkData(search) {
        try {
            const res = await fetch(`https://api.openbrewerydb.org/breweries?by_name=${encodeURIComponent(search)}`);
            const json = await res.json();
            return json;
        } catch (e) {
            console.error(e);
        }

        return null;
    }
};