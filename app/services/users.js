const db = require('../models');
const sha1 = require('sha1');
const config = require('dotenv').config().parsed;

module.exports.create = async (user={}) => {
    if (! user.email) {
        throw {msg: 'Informe o e-mail do usuário'};
    }
    if (! user.password) {
        throw {msg: 'Informe a senha do usuário'};
    }
    const newUser = await db.User.create(user);

    return newUser;
};

module.exports.retrieve = async (user={}) => {
    const users = await db.User.findAll({limit: 999, order: [['id', 'desc']]});
    return users;
};

module.exports.update = async (user={}) => {
    let userData = await db.User.findAll({where: {id: user.id}});
    if (! userData.length) {
        throw {msg: 'Usuário não existe'};
    }
    userData = userData.pop();
    await userData.update(user, ['name', 'email', 'birthday', 'password']);
    
    return userData;
};

module.exports.destroy = async (id) => {
    let userData = await db.User.findAll({where: {id}});
    if (! userData.length) {
        throw {msg: 'Usuário não existe'};
    }
    userData = userData.pop();
    await userData.destroy();

    return true;
};

module.exports.login = async login => {
    if (! (login.email && login.password)) {
        return null;
    }

    try {
        const users = await db.User.findAll({
            where: {
                email: login.email,
                password: login.password
            }
        });

        let user = users.pop();
        const token = sha1('ieiesalcifufu' + user.email + (new Date()));
        user = await user.update({
            token
        }, {fields: ['token']});

        return user;
    } catch (e) {
        console.error(e);
    }
    
    return null;
};

module.exports.checkToken = async token => {
    if (! token) {
        return null;
    }

    try {
        const users = await db.User.findAll({
            where: { token }
        });

        return users.pop();
    } catch (e) {
        console.error(e);
    }
    
    return null;
};
