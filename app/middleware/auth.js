const userSrv = require('../services/users')

const public = [
    '/login',
    '/'
];

const masterkey = '4c1b4ac06bba8f10fa4db4b68c857b437daed063';

module.exports = function (req, res, next) {
    const url = req.originalUrl;
    const token = req.get('Auth-Token');

    if (public.indexOf(url) !== -1) {
        return next();
    }

    if (token === masterkey) {
        return next();
    }

    return userSrv.checkToken(token).then(user => {
        if (! user) {
            return res.status(403).json({error: 'AUTH_REQUIRED'});
        }
        req.currentUser = user;

        next();
    });
}