const drikSrv = require('../services/drinks');

module.exports = app => {
    app.get('/beer/:search', (req, res) => {
        drikSrv.getDrinkData(req.params.search).then(beers => res.json(beers));        
    });
}