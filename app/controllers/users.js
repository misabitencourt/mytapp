const userSrv = require('../services/users');

module.exports = app => {
    app.get('/user', (req, res) => {
        userSrv.retrieve().then(users => {
            return res.json(users.map(user => ({
                id: user.id,
                name: user.name,
                email: user.email,
                birthday: user.birthday
            })));
        });
    });

    app.post('/user/', (req, res) => {        
        userSrv.create({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            birthday: req.body.birthday
        }).then(user => {
            return res.json(user);
        }).catch(e => res.status(500).json(e));
    });

    app.put('/user/:id', (req, res) => {        
        const id = req.originalUrl.split('/').pop()*1;
        userSrv.update({
            id,
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            birthday: req.body.birthday
        }).then(user => {
            return res.json(user);
        }).catch(e => res.status(500).json(e));
    });

    app.delete('/user/:id', (req, res) => {
        const id = req.originalUrl.split('/').pop()*1;
        if (isNaN(id)) {
            return res.status(400);
        }
        userSrv.destroy(id).then(() => {
            return res.json({ok: true});
        }).catch(e => res.status(500).json(e));
    });
}