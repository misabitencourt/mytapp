const userSrv = require('../services/users');

module.exports = app => {
    app.post('/login', (req, res) => {
        userSrv.login(req.body).then(user => {
            if (! user) {
                return res.status(403).json({error: 'INVALID_AUTH'});
            }
            
            delete user.password;
            res.json(user);
        });
    });
}