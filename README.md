# Mytapp Test

This project is an application test to a company called Mytapp. It is a NodeJS backend that list
beers from an public API with a local authentication.

# Stack

The backend was written in ES2017 in NodeJS platform. The frontend is written in vanilla ES2017 and it uses a bundler called Rollup. Material Bootstrap is used as CSS framework.

# Installing

To run up the backend in some machine, you will need a S.O. with NodeJS and Postgress installed.
In that machine, run ```npm install``` and ```npm start```. If you want to register new users, use
the Postman collection in the project root.

# Access Link

``` http://localhost:3000/web/ ```