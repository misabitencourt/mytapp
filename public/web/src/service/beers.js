import user from './user';

export default async (search='', page=1) => {
    const res = await fetch('/beer/' + encodeURIComponent(search), {
        headers: {
            'Auth-Token': user.token
        }
    });
    const json = await res.json();
    
    return json;
}