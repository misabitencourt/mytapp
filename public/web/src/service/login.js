

export const sendLogin = async login => {
    const res = await fetch('/login', {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(login)
    });

    if (res.status !== 200) {
        throw {msg: 'Incorrect credentials'};
    }

    const json = await res.json();
    sessionStorage.user = JSON.stringify(json);
}