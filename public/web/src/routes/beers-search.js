import el from '../utils/el';
import beersSearch from '../service/beers';

export default storage => {
    let listUl,
        searchInput;

    async function search() {
        const createBeerLi = beer => el('li', {className: 'list-group-item text-center'}, beer.name);
        let beers = await beersSearch(searchInput.value);
        console.log(beers);
        listUl.innerHTML = '';
        let sublist = beers.splice(0, 5);
        let beersLi = sublist.map(beer => createBeerLi(beer));
        if (! beersLi.length) {
            listUl.appendChild(el('li', {className: 'list-group-item text-center'}, 'No one beer found'))
        } else {
            beersLi.forEach(b => listUl.appendChild(b));
        }

        const addMore = () => {
            if (beers.length) {
                listUl.appendChild(el('li', {className: 'list-group-item'}, [
                    el('button', {className: 'btn btn-primary'}, 'More', [{
                        evt: 'click',
                        fn: e => {
                            e.target.parentElement.removeChild(e.target);
                            const newItems = beers.splice(0, 5);
                            sublist = sublist.concat(newItems);
                            console.log(newItems);
                            newItems.map(b => createBeerLi(b)).forEach(b => listUl.appendChild(b));
                            addMore();
                        }
                    }])
                ]));
            }
        };
        addMore();
    }

    return el('div', {className: ''}, [
        el('div', {className: 'row'}, [
            el('div', {className: 'col-md-4'}, []),
            el('div', {className: 'col-md-4 mt-5'}, [
                el('h3', {}, 'Brewery search'),
                el('div', {className: 'row'}, [
                    el('div', {className: 'col-md-8 pr-0'}, [
                        searchInput = el('input', {className: 'form-control', placeholder: 'Brewery name'}, [], [{
                            evt: 'keyup',
                            fn: e => e.keyCode === 13 && search()
                        }])
                    ]),
                    el('div', {className: 'col-md-4 pl-0'}, [
                        el('button', {className: 'btn btn-primary bg-primary text-white'}, 'Search', [{
                            evt: 'click',
                            fn: search
                        }])
                    ])
                ])
            ]),
            el('div', {className: 'col-md-4'}, [])
        ]),

        el('div', {className: 'row'}, [
            el('div', {className: 'col-md-4'}, []),
            el('div', {className: 'col-md-4'}, [
                listUl = el('ul', {className: 'list-group'}, [])
            ]),
            el('div', {className: 'col-md-4'}, [])
        ])
    ]);
}