import el from '../utils/el';
import {sendLogin} from '../service/login';

export default storage => {
    let cardBody;
    const loginEl = el('div', {}, [
        el('div', {className: 'row mt-5'}, [
            el('div', {className: 'col-md-4'}),
            el('div', {className: 'col-md-4'}, [
                el('div', {className: 'card'}, [
                    el('div', {className: 'card-header bg-primary text-white'}, 'Login'),
                    cardBody = el('div', {className: 'card-body'}, [
                        el('div', {className: 'form-group'}, [
                            el('input', {type: 'email', placeholder: 'E-mail', className: 'form-control'}, [], [{
                                evt: 'change',
                                fn: e => storage.model.email = e.target.value
                            }])
                        ]),
                        el('div', {className: 'form-group'}, [
                            el('input', {type: 'password', placeholder: 'Password', className: 'form-control'}, [], [{
                                evt: 'change',
                                fn: e => storage.model.password = e.target.value
                            }])
                        ]),
                        el('div', {className: 'mt-3'}, [
                            el('button', {type: 'button', className: 'btn btn-primary btn-block'}, 'Sign in', [{
                                evt: 'click',
                                fn: async e => {
                                    const originalText = e.target.textContent;
                                    e.target.disabled = true;
                                    e.target.textContent = 'Enviando...';
                                    try {
                                        await sendLogin(storage.model);
                                        return window.location.reload();
                                    } catch (e) {
                                        const msg = el('div', {className: 'alert alert-danger'}, e.msg);
                                        cardBody.appendChild(msg);
                                        setTimeout(() => cardBody.removeChild(msg), 5e3);
                                    }
                                    e.target.disabled = false;
                                    e.target.textContent = originalText;
                                }
                            }])
                        ])
                    ])
                ])
            ]),
            el('div', {className: 'col-md-4'})
        ])
    ]);
 
    return loginEl;
}

