

export default (tag, props=null, children=[], events=[]) => {
    const element = document.createElement(tag);
    for (let i in props) {
        element[i] = props[i];
    }
    
    if (typeof(children) === 'string') {
        element.textContent = children;
    } else {
        for (let child of children) {
            element.appendChild(child);
        }
    }

    if (events && events.length) {
        for (let evt of events) {
            element.addEventListener(evt.evt, evt.fn);
        }
    }

    return element;
}