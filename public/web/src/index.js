import login from './routes/login';
import beerSearch from './routes/beers-search';

function render(parent) {
    const storage = {
        model: {}
    };
    
    let element;
    if (! sessionStorage.user) {
        element = login(storage);
    } else {
        element = beerSearch(storage);
    }

    parent.innerHTML = '';
    parent.appendChild(element);
}

const parent = document.getElementById('app');
render(parent);




