(function () {
    'use strict';

    function __async(g) {
      return new Promise(function (s, j) {
        function c(a, x) {
          try {
            var r = g[x ? "throw" : "next"](a);
          } catch (e) {
            j(e);return;
          }r.done ? s(r.value) : Promise.resolve(r.value).then(c, d);
        }function d(e) {
          c(e, 1);
        }c();
      });
    }

    var el = ((tag, props = null, children = [], events = []) => {
        const element = document.createElement(tag);
        for (let i in props) {
            element[i] = props[i];
        }

        if (typeof children === 'string') {
            element.textContent = children;
        } else {
            for (let child of children) {
                element.appendChild(child);
            }
        }

        if (events && events.length) {
            for (let evt of events) {
                element.addEventListener(evt.evt, evt.fn);
            }
        }

        return element;
    });

    const sendLogin = login => __async(function* () {
        const res = yield fetch('/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(login)
        });

        if (res.status !== 200) {
            throw { msg: 'Incorrect credentials' };
        }

        const json = yield res.json();
        sessionStorage.user = JSON.stringify(json);
    }());

    var login = (storage => {
        let cardBody;
        const loginEl = el('div', {}, [el('div', { className: 'row mt-5' }, [el('div', { className: 'col-md-4' }), el('div', { className: 'col-md-4' }, [el('div', { className: 'card' }, [el('div', { className: 'card-header bg-primary text-white' }, 'Login'), cardBody = el('div', { className: 'card-body' }, [el('div', { className: 'form-group' }, [el('input', { type: 'email', placeholder: 'E-mail', className: 'form-control' }, [], [{
            evt: 'change',
            fn: e => storage.model.email = e.target.value
        }])]), el('div', { className: 'form-group' }, [el('input', { type: 'password', placeholder: 'Password', className: 'form-control' }, [], [{
            evt: 'change',
            fn: e => storage.model.password = e.target.value
        }])]), el('div', { className: 'mt-3' }, [el('button', { type: 'button', className: 'btn btn-primary btn-block' }, 'Sign in', [{
            evt: 'click',
            fn: e => __async(function* () {
                const originalText = e.target.textContent;
                e.target.disabled = true;
                e.target.textContent = 'Enviando...';
                try {
                    yield sendLogin(storage.model);
                    return window.location.reload();
                } catch (e) {
                    const msg = el('div', { className: 'alert alert-danger' }, e.msg);
                    cardBody.appendChild(msg);
                    setTimeout(() => cardBody.removeChild(msg), 5e3);
                }
                e.target.disabled = false;
                e.target.textContent = originalText;
            }())
        }])])])])]), el('div', { className: 'col-md-4' })])]);

        return loginEl;
    });

    const user = JSON.parse(sessionStorage.user || '{}');

    var beersSearch = ((search = '', page = 1) => __async(function* () {
        const res = yield fetch('/beer/' + encodeURIComponent(search), {
            headers: {
                'Auth-Token': user.token
            }
        });
        const json = yield res.json();

        return json;
    }()));

    var beerSearch = (storage => {
        let listUl, searchInput;

        function search() {
            return __async(function* () {
                const createBeerLi = beer => el('li', { className: 'list-group-item text-center' }, beer.name);
                let beers = yield beersSearch(searchInput.value);
                console.log(beers);
                listUl.innerHTML = '';
                let sublist = beers.splice(0, 5);
                let beersLi = sublist.map(beer => createBeerLi(beer));
                if (!beersLi.length) {
                    listUl.appendChild(el('li', { className: 'list-group-item text-center' }, 'No one beer found'));
                } else {
                    beersLi.forEach(b => listUl.appendChild(b));
                }

                const addMore = () => {
                    if (beers.length) {
                        listUl.appendChild(el('li', { className: 'list-group-item' }, [el('button', { className: 'btn btn-primary' }, 'More', [{
                            evt: 'click',
                            fn: e => {
                                e.target.parentElement.removeChild(e.target);
                                const newItems = beers.splice(0, 5);
                                sublist = sublist.concat(newItems);
                                console.log(newItems);
                                newItems.map(b => createBeerLi(b)).forEach(b => listUl.appendChild(b));
                                addMore();
                            }
                        }])]));
                    }
                };
                addMore();
            }());
        }

        return el('div', { className: '' }, [el('div', { className: 'row' }, [el('div', { className: 'col-md-4' }, []), el('div', { className: 'col-md-4 mt-5' }, [el('h3', {}, 'Brewery search'), el('div', { className: 'row' }, [el('div', { className: 'col-md-8 pr-0' }, [searchInput = el('input', { className: 'form-control', placeholder: 'Brewery name' }, [], [{
            evt: 'keyup',
            fn: e => e.keyCode === 13 && search()
        }])]), el('div', { className: 'col-md-4 pl-0' }, [el('button', { className: 'btn btn-primary bg-primary text-white' }, 'Search', [{
            evt: 'click',
            fn: search
        }])])])]), el('div', { className: 'col-md-4' }, [])]), el('div', { className: 'row' }, [el('div', { className: 'col-md-4' }, []), el('div', { className: 'col-md-4' }, [listUl = el('ul', { className: 'list-group' }, [])]), el('div', { className: 'col-md-4' }, [])])]);
    });

    function render(parent) {
        const storage = {
            model: {}
        };

        let element;
        if (!sessionStorage.user) {
            element = login(storage);
        } else {
            element = beerSearch(storage);
        }

        parent.innerHTML = '';
        parent.appendChild(element);
    }

    const parent = document.getElementById('app');
    render(parent);

}());
//# sourceMappingURL=bundle.js.map
